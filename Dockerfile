FROM nginx:1.12

# Nginx Config
COPY config/nginx.conf /etc/nginx/nginx.conf

# Site Vhost config
COPY conf.d /etc/nginx/conf.d
COPY sites-available /etc/nginx/sites-available
#COPY scripts /etc/nginx/scripts
RUN mkdir /etc/nginx/sites-enabled

# Install Vim - can be removed once we're confident with this image
RUN apt-get update && apt-get install -y vim

# Change the UID of nginx for OSX writing permission problem
RUN usermod -u 1700 nginx

# -----------------------------------------------------------------------------
# DEFAULT ENVIRONMENT VARIABLES
# -----------------------------------------------------------------------------
ENV NGINX_CONF dev
ENV SITE_TYPE default

# -----------------------------------------------------------------------------
# Copy Run entry script
# -----------------------------------------------------------------------------
COPY run.sh /run.sh
RUN chmod 755 /run.sh

CMD ["/run.sh"]

RUN mkdir -p /var/log/nginx
COPY ./error.log /var/log/nginx/error.log
RUN  chmod 777 /var/log